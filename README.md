![BashNinja's Bash Game](bashgame.png "BashNinja's Bash Game")

# How to build

```
./build-container.sh
```

# How to run

```
docker run -d --name sonofhonor -p 4222:22 sonofhonor
```

# How to start the game

`ssh neverlan@127.0.0.1 -p 4222`

Password: `neverlan`

# Troubleshooting

## Too many auths
If you see something like this:
```
➜  ~ ssh neverlan@127.0.0.1 -p 4222
###############################################################
#                   Welcome to my game                        # 
#         Check out https://neverlanctf.com for info          #
#               Make sure you read the rules                  #
#         https://neverlanctf.com/docs/code-of-conduct        #
###############################################################
Received disconnect from 127.0.0.1 port 4222:2: Too many authentication failures
Disconnected from 127.0.0.1 port 4222
```
It probably is happening because you have a lot of SSH keys.

Try using `ssh -o PreferredAuthentications=password neverlan@127.0.0.1 -p 4222`. 
