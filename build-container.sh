#!/bin/bash

D="sonofhonor"

echo "Building ${D}"


printf "Building ${D}..."
docker build -q -t ${D} .
if [[ $? -ne 0 ]]; then
    printf "failed to build \"${D}\" - cannot continue\n"
    exit
fi
