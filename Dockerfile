FROM  ubuntu:16.04
LABEL maintainer="bashNinja <mike@thebash.ninja>"

# Fix Locale
RUN apt-get update && \
    apt-get install -y locales locales-all && \
    locale-gen en_US.UTF-8 && \
    update-locale LANG=en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN apt-get install -y openssh-server coreutils binutils

# Install Tools
RUN apt-get install -y vim nano 

# Maybe install these? Big
# RUN apt-get install -y binwalk gdb

# Maybe install these? Small
# RUN apt-get install -y foremost exiftool ltrace strace
# RUN pip install hashid

# Install banner sillyness
RUN apt-get install -y lolcat cowsay toilet figlet

RUN mkdir /var/run/sshd

RUN echo "alias su=\"su -l \"" >> /etc/bash.bashrc && \
    echo "/etc/bashgames" >> /etc/bash.bashrc && \
    echo "ClientAliveInterval 300\nClientAliveCountMax 0" >> /etc/ssh/sshd_config && \
    echo "Banner /etc/banner" >> /etc/ssh/sshd_config && \
    sed -i 's/PrintLastLog yes/PrintLastLog no/g' /etc/ssh/sshd_config

RUN rm /etc/update-motd.d/00-header  && \
    rm /etc/update-motd.d/10-help-text

RUN useradd -s /bin/bash -m neverlan && \
    useradd -s /bin/bash -m level1 && \
    useradd -s /bin/bash -m level2 && \
    useradd -s /bin/bash -m level3 && \
    useradd -s /bin/bash -m level4 && \
    useradd -s /bin/bash -m level5 && \
    useradd -s /bin/bash -m level6 && \
    useradd -s /bin/bash -m level7 && \
    useradd -s /bin/bash -m level8 && \
    useradd -s /bin/bash -m level9

RUN echo 'root:theretostasiscampsiteforwentsauce' | chpasswd && \
    echo 'neverlan:neverlan' | chpasswd && \
    echo 'level1:act-with-honor-and-honor-will-aid-you' | chpasswd && \
    echo 'level2:the-only-path-to-honor-is-to-stick-to-your-chosen-code' | chpasswd && \
    echo 'level3:child-of-honor' | chpasswd && \
    echo 'level4:only*hack^things%you$own' | chpasswd && \
    echo 'level5:on-my-honor-i-will-do-my-best' | chpasswd && \
    echo 'level6:have-you-memorized-the-code-yet' | chpasswd && \
    echo 'level7:white-hats-have-values-and-rules' | chpasswd && \
    echo 'level8:my-wit-ran-out-5-levels-ago' | chpasswd && \
    echo 'level9:please-someone-help' | chpasswd

COPY root/ /

RUN chown -R neverlan:neverlan /home/neverlan/    && \
    chown -R level1:level1 /home/level1/ && \
    chown -R level2:level2 /home/level2/ && \
    chown -R level3:level3 /home/level3/ && \
    chown -R level4:level4 /home/level4/ && \
    chown -R level5:level5 /home/level5/ && \
    chown -R level6:level6 /home/level6/ && \
    chown -R level7:level7 /home/level7/ && \
    chown -R level8:level8 /home/level8/ && \
    chown -R level9:level9 /home/level9/ && \
    chmod -R 600 /home/*/ && chmod 755 /home/*

EXPOSE 22

CMD ["/usr/sbin/sshd", "-D"]
